## 获取数字资产充值记录
```request
GET https://trade.zb.cn/api/getChargeRecord?accesskey=youraccesskey&currency=btc&method=getChargeRecord&pageIndex=1&pageSize=10&sign=请求加密签名串&reqTime=当前时间毫秒数
```
```response
{
    "code": 1000,
    "message": {
        "des": "success",
        "isSuc": true,
        "datas": {
            "list": [
                {
                    "address": "1FKN1DZqCm8HaTujDioRL2Aezdh7Qj7xxx",
                    "amount": "1.00000000",
                    "confirmTimes": 1,
                    "currency": "BTC",
                    "description": "确认成功",
                    "hash": "7ce842de187c379abafadd64a5fe66c5c61c8a21fb04edff9532234a1dae6xxx",
                    "id": 558,
                    "itransfer": 1,
                    "status": 2,
                    "submit_time": "2016-12-07 18:51:57"
                }...
            ],
            "pageIndex": 1,
            "pageSize": 10,
            "total": 8
        }
    }
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
method | String | 直接赋值getChargeRecord
accesskey | String | accesskey
currency | String | 币种
pageIndex | int | 当前页数
pageSize | int | 每页数量
sign | String | 请求加密签名串
reqTime | long | 当前时间毫秒数

```resdata
code : 返回代码
message : 提示信息
amount : 充值金额
confirmTimes : 充值确认次数
currency : 充值货币类型(大写)
description : 充值记录状态描述
hash : 充值交易号
id : 充值记录id
itransfer : 是否内部转账，1是0否
status : 状态(0等待确认，1充值失败，2充值成功)
submit_time : 充值时间
address : 充值地址
```

```java
public void getChargeRecord() {
	//测试apiKey:ce2a18e0-dshs-4c44-4515-9aca67dd706e
	//测试secretKey:c11a122s-dshs-shsa-4515-954a67dd706e
	//加密类:https://github.com/zb2017/api/blob/master/zb_netty_client_java/src/main/java/websocketx/client/EncryDigestUtil.java
	//secretKey通过sha1加密得到:86429c69799d3d6ac5da5c2c514baa874d75a4ba
	String digest = EncryDigestUtil.digest("c11a122s-dshs-shsa-4515-954a67dd706e");
	//参数按照ASCII值排序
	String paramStr = "accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch&method=getChargeRecord&pageIndex=1&pageSize=10";
	//sign通过HmacMD5加密得到:e871dcce5138334704165ee53efe5545
	String sign = EncryDigestUtil.hmacSign(paramStr, digest);
	//组装最终发送的请求url
	String finalUrl = "https://trade.zb.cn/api/getChargeRecord?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch&method=getChargeRecord&pageIndex=1&pageSize=10&reqTime=1539942326084&sign=e871dcce5138334704165ee53efe5545";
	//取得返回结果json
	String resultJson = HttpRequest.get(finalUrl).send().bodyText();
}
```

```python
import hashlib
import struct
import time
import requests

class ZApi:
    def __init__(self, access_key, secret_key):
        self._access_key_ = access_key
        self._secret_key_ = secret_key
        self._markets_ = self.markets()
        if len(self._markets_) < 1:
            raise Exception("Get markets status failed")

    def getChargeRecord(self, market, type, amount, price):
        MyUrl = 'https://trade.zb.cn/api/getChargeRecord'
        params = 'accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch&method=getChargeRecord&pageIndex=1&pageSize=10'
        return self.call_api(url=MyUrl, params=params)

    def call_api(self, url, params=''):
        full_url = url
        if params:
            #sha_secret=86429c69799d3d6ac5da5c2c514baa874d75a4ba
            sha_secret = self.digest(self._secret_key_)
            #sign = e871dcce5138334704165ee53efe5545
            sign = self.hmac_sign(params, sha_secret)
            req_time = int(round(time.time() * 1000))
            params += '&sign=%s&reqTime=%d' % (sign, req_time)
            #full_url = https://trade.zb.cn/api/getChargeRecord?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&currency=bch&method=getChargeRecord&pageIndex=1&pageSize=10&reqTime=1540295995596&sign=e871dcce5138334704165ee53efe5545
            full_url += '?' + params
        result = {}
        while True:
            try:
                r = requests.get(full_url, timeout=2)
            except Exception:
                time.sleep(0.5)
                continue
            if r.status_code != 200:
                time.sleep(0.5)
                r.close()
                continue
            else:
                result = r.json()
                r.close()
                break
        return result

    @staticmethod
    def fill(value, lenght, fill_byte):
        if len(value) >= lenght:
            return value
        else:
            fill_size = lenght - len(value)
        return value + chr(fill_byte) * fill_size

    @staticmethod
    def xor(s, value):
        slist = list(s.decode('utf-8'))
        for index in range(len(slist)):
            slist[index] = chr(ord(slist[index]) ^ value)
        return "".join(slist)

    def hmac_sign(self, arg_value, arg_key):
        keyb = struct.pack("%ds" % len(arg_key), arg_key.encode('utf-8'))
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        k_ipad = self.xor(keyb, 0x36)
        k_opad = self.xor(keyb, 0x5c)
        k_ipad = self.fill(k_ipad, 64, 54)
        k_opad = self.fill(k_opad, 64, 92)
        m = hashlib.md5()
        m.update(k_ipad.encode('utf-8'))
        m.update(value)
        dg = m.digest()

        m = hashlib.md5()
        m.update(k_opad.encode('utf-8'))
        subStr = dg[0:16]
        m.update(subStr)
        dg = m.hexdigest()
        return dg
  
    def digest(self, arg_value):
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        h = hashlib.sha1()
        h.update(value)
        dg = h.hexdigest()
        return dg
```